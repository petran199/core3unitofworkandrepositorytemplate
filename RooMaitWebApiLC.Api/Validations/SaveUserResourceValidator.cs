﻿using FluentValidation;
using RooMaitWebApiLC.Api.Resources;

namespace RooMaitWebApiLC.Api.Validations
{
    public class SaveUserResourceValidator : AbstractValidator<SaveUserResource>
    {
        public SaveUserResourceValidator()
        {
            RuleFor(a => a.FirstName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(a => a.LastName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(a => a.Email)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
