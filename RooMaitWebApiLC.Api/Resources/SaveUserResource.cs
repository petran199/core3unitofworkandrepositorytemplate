﻿namespace RooMaitWebApiLC.Api.Resources
{
    public class SaveUserResource
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
