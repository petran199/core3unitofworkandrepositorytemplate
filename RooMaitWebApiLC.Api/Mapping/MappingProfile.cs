﻿using AutoMapper;
using RooMaitWebApiLC.Api.Resources;
using RooMaitWebApiLC.Core.Models;

namespace RooMaitWebApiLC.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to Resource
            CreateMap<User, UserResource>();

            // Resource to Domain
            CreateMap<UserResource, User>();
        }
    }
}
