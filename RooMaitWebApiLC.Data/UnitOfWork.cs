﻿using System.Threading.Tasks;
using RooMaitWebApiLC.Core;
using RooMaitWebApiLC.Core.Repositories;
using RooMaitWebApiLC.Data.Repositories;

namespace RooMaitWebApiLC.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RooMaitWebApiLcDbContext _context;
        private UserRepository _userRepository;

        public UnitOfWork(RooMaitWebApiLcDbContext context)
        {
            _context = context;
        }

        public IUserRepository Users => _userRepository ??= new UserRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}