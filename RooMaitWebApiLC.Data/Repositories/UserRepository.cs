﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RooMaitWebApiLC.Core.Models;
using RooMaitWebApiLC.Core.Repositories;

namespace RooMaitWebApiLC.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(RooMaitWebApiLcDbContext context)
            : base(context)
        { }

        private RooMaitWebApiLcDbContext RooMaitWebApiLcDbContext
        {
            get { return Context as RooMaitWebApiLcDbContext; }
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await RooMaitWebApiLcDbContext.Users
                .ToListAsync();
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await RooMaitWebApiLcDbContext.Users
                .SingleOrDefaultAsync(m => m.Id == id); 
        }
    }
}