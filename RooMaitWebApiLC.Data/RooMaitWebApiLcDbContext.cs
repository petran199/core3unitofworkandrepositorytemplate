﻿using System;
using Microsoft.EntityFrameworkCore;
using RooMaitWebApiLC.Core.Models;
using RooMaitWebApiLC.Data.Configurations;

namespace RooMaitWebApiLC.Data
{
    public class RooMaitWebApiLcDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public RooMaitWebApiLcDbContext(DbContextOptions<RooMaitWebApiLcDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new UserConfiguration());
        }

    }
}
