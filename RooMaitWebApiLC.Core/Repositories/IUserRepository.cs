﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RooMaitWebApiLC.Core.Models;

namespace RooMaitWebApiLC.Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<User>> GetAllAsync();
        Task<User> GetByIdAsync(int id);
    }
}
