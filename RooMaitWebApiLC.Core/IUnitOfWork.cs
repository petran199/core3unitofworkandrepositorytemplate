﻿using System;
using System.Threading.Tasks;
using RooMaitWebApiLC.Core.Repositories;

namespace RooMaitWebApiLC.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        Task<int> CommitAsync();
    }
}
